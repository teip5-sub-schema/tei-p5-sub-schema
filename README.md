# TEI-P5 Anwendungsprofil "SUB Schema"
> TEI Customization der Niedersächsischen Staats- und Universitätsbibliothek

## Download
- **GitLab**: clone this Repository from GitLab using the "Clone" Button 
- **Git**: 
  - Using HTTPS: `git clone https://gitlab.gwdg.de/teip5-sub-schema/tei-p5-sub-schema.git`
  - Using SSH: `git clone git@gitlab.gwdg.de:teip5-sub-schema/tei-p5-sub-schema.git`


## Abstract
SUB Schema is a TEI-P5 Customisation used by digital editions projects supported by the SUB Göttingen.


## Documentation
The official german documentation is still a DRAFT! Visit it anyway on [http://teip5-sub-schema.pages.gwdg.de/tei-p5-sub-schema/p5sub_schema.html](http://teip5-sub-schema.pages.gwdg.de/tei-p5-sub-schema/p5sub_schema.html)


## How to use?
If you want to keep it simple copy these Processing Instructions into an empty XML-File and go:

```xml
<?xml-model href="https://gitlab.gwdg.de/teip5-sub-schema/tei-p5-sub-schema/-/raw/master/schema/rng/p5sub_schema.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/teip5-sub-schema/tei-p5-sub-schema/-/raw/master/schema/rng/p5sub_schema.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
```
These Instructions reference the RNG schema and its schematron validation routines from the GitLab. This (and an online connection) is everything to start working with SUB Schema in oXygen.


## Licence and Contact
SUB Schema is published under CC BY-SA Licence.

Questions, remarks or other kinds of comments?: Uwe Sikora, SUB Göttingen [<sikora@sub.uni-goettingen.de>](mailto:sikora@sub.uni-goettingen.de)