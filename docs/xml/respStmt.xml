<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Verantwortlichkeitsvermerk</title>
                <author>Uwe Sikora</author>
            </titleStmt>
            <publicationStmt>
                <authority xml:id="UwS">Uwe Sikora</authority>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by/4.0"/>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Separate SUB-Schema docs-file</p>
            </sourceDesc>
        </fileDesc>
        <revisionDesc status="published">
            <change who="#UwS" when="2023-11-20">Ready to publish.</change>
            <change who="#UwS" when="2023-11-18">Linked to Schema ODD.</change>
            <change who="#UwS" when="2023-11-18">Created File.</change>
            <change who="#UwS" when="2022-01-24">Created Content (Intranet).</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div xml:id="doc-respStmt">
                <head>Verantwortlichkeitsvermerk</head>
                <table>
                    <row  role="label">
                        <cell>Element</cell>
                        <cell>Kommentar</cell>
                        <cell>Verpflichtungsgrad</cell>
                        <cell>Kardinalität</cell>
                        <cell>Attribute</cell>
                    </row>
                    <row>
                        <cell><ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-respStmt.html">respStmt</ref></cell>
                        <cell>Enthält Angaben zur Verantwortlichkeit einer Körperschaft.</cell>
                        <cell>optional</cell>
                        <cell>0..*</cell>
                        <cell/>
                    </row>
                    <row>
                        <cell>respStmt/<ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-resp.html">resp</ref></cell>
                        <cell>Enthält die Verantwortlichkeitsrolle einer Körperschaft.</cell>
                        <cell>verpflichtend (falls tei:respStmt gesetzt)</cell>
                        <cell>1</cell>
                        <cell>
                            <table>
                                <row>
                                    <cell>Attribut</cell>
                                    <cell>Kommentar</cell>
                                    <cell>Verpflichtungsgrad</cell>
                                    <cell>Datentyp</cell>
                                </row>
                                <row>
                                    <cell>@<hi rend="italic">ref</hi>
                                    </cell>
                                    <cell>Angabe eines URI zur Identifikation eines Bezeichners (oder einer Normdatei, die den Bezeichner
                                        definiert)</cell>
                                    <cell>optional, aber empfohlen</cell>
                                    <cell>1..* xs:anyURI (vorzugsweise aus dem Vokabular <ref target="http://id.loc.gov/vocabulary/relators"
                                            >http://id.loc.gov/vocabulary/relators</ref>)</cell>
                                </row>
                                <row>
                                    <cell>@<hi rend="italic">key</hi>
                                    </cell>
                                    <cell>Angabe eines Bezeichners aus einer Normdatei</cell>
                                    <cell>optional, sofern kein URI zur Identifikation des Bezeichners existiert.</cell>
                                    <cell>0..1 xs:string</cell>
                                </row>
                            </table>
                        </cell>
                    </row>
                    <row>
                        <cell>respStmt/<ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-name.html">name</ref></cell>
                        <cell>Enthält den Namen der Körperschaft. Es können mehrere Namen für eine Körperschaft angegeben werden.</cell>
                        <cell>verpflichtend (falls tei:respStmt gesetzt)</cell>
                        <cell>1..*</cell>
                        <cell/>
                    </row>
                </table>
                <egXML xmlns="http://www.tei-c.org/ns/Examples">
                    <respStmt>
                        <resp ref="[URI zur Identifikation der Rolle durch einen Bezeichner]"
                            key="[Bezeichner aus Vokabular, falls nicht bereits durch URI in @ref spezifiziert.]">[Rolle]</resp>
                        <name xml:lang="[Sprachcode gemäß BCP47]">[Eigenname]</name>
                    </respStmt>
                </egXML>
                <p>Verantwortlichkeitsvermerke werden via <gi rend="bold">respStmt</gi> ausgezeichnet, unter obligatorischer Angabe der <hi
                        rend="bold">Verantwortungsrolle</hi> in <gi rend="bold">resp</gi> und des <hi rend="bold">Namens der verantwortlichen
                        Körperschaft</hi> in <gi rend="bold">name</gi>. <specList>
                        <specDesc key="respStmt"/>
                    </specList></p>
                <p>Es wird empfohlen, Verantwortlichkeitsrollen mit Bezeichnern aus einer Normdatei eindeutig zu spezifizieren. Hierzu bieten sich die
                    Deskriptoren des <ref target="http://id.loc.gov/vocabulary/relators">Relators Vokabulars der Library of Congress</ref> an. </p>
                <p>Sofern ein Deskriptor direkt durch einen URI identifiziert werden kann, wird der URI in <gi>resp</gi>/<att rend="italic">ref</att>
                    angegeben. Sofern ausschließlich die Normdatei durch einen URI identifiziert werden kann, dient <gi rend="bold">resp</gi>/<att
                        rend="italic">ref</att> zur Angabe des URIs der Normdatei und <gi>resp</gi>/<att rend="italic">key</att> zur Angabe des
                    Bezeichners:</p>
                <egXML xmlns="http://www.tei-c.org/ns/Examples">
                    <respStmt>
                        <resp ref="https://id.loc.gov/vocabulary/relators/dtm.html">Metadata Support &amp; Dataconversion</resp>
                        <name>Uwe Sikora</name>
                    </respStmt>
                </egXML>

                <ab rend="head">Vereinheitlichungen &amp; Mapping</ab>
                <p>Elemente aus <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-model.respLike.html">tei.model.respLike</ref>
                    (ausgenommen sind <gi>author</gi>, <gi>editor</gi>, <gi>funder</gi> und <gi>meeting</gi>) werden wie folgt in Hinblick auf
                        <gi>respStmt</gi> vereinheitlicht:</p>
                <table>
                    <row  role="label">
                        <cell>target</cell>
                        <cell>result</cell>
                        <cell>XML</cell>
                    </row>
                    <row>
                        <cell>
                            <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-principal.html">principal</ref></cell>
                        <cell>
                            <p>respStmt/resp</p>
                            <p>[@ref="http://id.loc.gov/vocabulary/relators/rth"]</p>
                            <p>[text()="Research Team Head"]</p>
                        </cell>
                        <cell>
                            <egXML xmlns="http://www.tei-c.org/ns/Examples">
                                <respStmt>
                                    <resp ref="http://id.loc.gov/vocabulary/relators/rth">Research Team Head</resp>
                                    <name xml:lang="[Sprachcode gemäß BCP47]">[Eigenname]</name>
                                </respStmt>
                            </egXML>
                        </cell>
                    </row>
                    <row>
                        <cell>
                            <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-sponsor.html">sponsor</ref></cell>
                        <cell>
                            <p>respStmt/resp</p>
                            <p>[@ref="http://id.loc.gov/vocabulary/relators/spn"]</p>
                            <p>[text()="Sponsor"]</p>
                        </cell>
                        <cell>
                            <egXML xmlns="http://www.tei-c.org/ns/Examples">
                                <respStmt>
                                    <resp ref="http://id.loc.gov/vocabulary/relators/spn">Sponsor</resp>
                                    <name xml:lang="[Sprachcode gemäß BCP47]">[Eigenname]</name>
                                </respStmt>
                            </egXML>
                        </cell>
                    </row>
                </table>
            </div>
        </body>
    </text>
</TEI>
