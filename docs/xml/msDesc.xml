<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Manuskriptbeschreibung (msDesc)</title>
                <author>Uwe Sikora</author>
            </titleStmt>
            <publicationStmt>
                <authority xml:id="UwS">Uwe Sikora</authority>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by/4.0"/>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Separate SUB-Schema docs-file</p>
            </sourceDesc>
        </fileDesc>
        <revisionDesc status="candidate">
            <change who="#UwS" when="2023-11-18">Reached candidate Status.</change>
            <change who="#UwS" when="2023-11-18">Linked to Schema ODD.</change>
            <change who="#UwS" when="2023-11-18">Created File.</change>
            <change who="#UwS" when="2022-01-24">Created Content (Intranet).</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div xml:id="doc-msDesc">
                <head>Quellenbeschreibung bei unikalen Quellen (Manuskriptbeschreibung)</head>
                <table>
                    <row role="label">
                        <cell>Element</cell>
                        <cell>Kommentar</cell>
                        <cell>Verpflichtungsgrad</cell>
                        <cell>Kardinalität</cell>
                        <cell>DFG-DH</cell>
                        <cell>DTA-BF</cell>
                        <cell>TEI-Simple</cell>
                    </row>
                    <row>
                        <cell>
                            <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-msDesc.html">tei:msDesc</ref></cell>
                        <cell>Enthält die Handschriftenbeschreibung</cell>
                        <cell>optional</cell>
                        <cell>0..1</cell>
                        <cell>ja</cell>
                        <cell>ja</cell>
                        <cell>nein</cell>
                    </row>
                </table>
                <p>Im Fallen von Handschrifteneditionen oder, sofern eine unikale Quelle vorhanden ist, werden Informationen zur Quelle in
                        <gi>sourceDesc</gi>/<gi>msDesc</gi> erschlossen. Das SUB TEI-P5 Anwenungsprofil folgt hier weitestgehend den Vorgaben des <ref
                        target="https://dfg-viewer.de/fileadmin/groups/dfgviewer/TEI-Anwendungsprofil_1.0.pdf">DFG-DH</ref> in Hinblick auf die <hi
                        rend="bold">Handschriftenidentifikation und -beschreibung</hi>. </p>
                <p>Zur bibliographischen Erschließung nicht-unikaler Quellen siehe Kap. <ref target="#SpezifikationMetadatenbereich-bio_non_unikal"
                        >Quellenbeschreibung bei nicht-unikalen Quellen</ref>.</p>
                <egXML xmlns="http://www.tei-c.org/ns/Examples">
                    <sourceDesc>
                        <msDesc>
                            <msIdentifier>[Identifikatoren der Handschrift/Quelle]</msIdentifier>
                            <head>
                                <title>[Offizieller bibliogrphischer Titel der Handschrift/Quelle]</title>
                            </head>
                            <msContents>[Inhaltlicher Informationen zur Handschrift/Quelle]</msContents>
                            <physDesc>[Physische Informationen zur Handschrift/Quelle]</physDesc>
                            <history>[Provenienzinformationen zur Handschrift/Quelle]</history>
                        </msDesc>
                    </sourceDesc>
                </egXML>

                <!-- msIdentifier: Identifikatoren der unikalen Quelle -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="msIdentifier.xml" xpointer="element(doc-msIdentifier)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
                
                <!-- msIdentifier/head: Bibliographischer Titelvermerk zur unikalen Quelle -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="msDesc-head.xml" xpointer="element(doc-msDesc-head)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
                
                <!-- msContents: Beschreibung der Inhalte der unikalen Quelle -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="msContents.xml" xpointer="element(doc-msContents)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
                
                <!-- physDesc: Physische Beschreibung der unikalen Quelle -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="physDesc.xml" xpointer="element(doc-physDesc)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
                
                <!-- history: Provenienzbeschreibung zur unikalen Quelle -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="history.xml" xpointer="element(doc-history)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
            </div>
        </body>
    </text>
</TEI>
