<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Quellenbeschreibung bei nicht-unikalen Quellen</title>
                <author>Uwe Sikora</author>
            </titleStmt>
            <publicationStmt>
                <authority xml:id="UwS">Uwe Sikora</authority>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by/4.0"/>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Separate SUB-Schema docs-file</p>
            </sourceDesc>
        </fileDesc>
        <revisionDesc status="published">
            <change who="#UwS" when="2023-11-20">Reached published Status.</change>
            <change who="#UwS" when="2023-11-18">Reached candidate Status.</change>
            <change who="#UwS" when="2023-11-18">Linked to Schema ODD.</change>
            <change who="#UwS" when="2023-11-18">Created File.</change>
            <change who="#UwS" when="2022-01-24">Created Content (Intranet).</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div xml:id="doc-non-unikal-source">
                <head>Quellenbeschreibung bei nicht-unikalen Quellen</head>
                <table>
                    <row role="label">
                        <cell>Element</cell>
                        <cell>Kommentar</cell>
                        <cell>Verpflichtungsgrad</cell>
                        <cell>Kardinalität</cell>
                        <cell>DFG-DH</cell>
                        <cell>DTA-BF</cell>
                        <cell>TEI-Simple</cell>
                    </row>
                    <row>
                        <cell>
                            <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-bibl.html">tei:bibl</ref></cell>
                        <cell>Enthält eine bibliographische Kurzangabe zur Quelle</cell>
                        <cell>verplichtend (bei nicht-unikalen Quellen)</cell>
                        <cell>1</cell>
                        <cell>nein</cell>
                        <cell>ja</cell>
                        <cell>nein</cell>
                    </row>
                    <row>
                        <cell>
                            <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-biblFull.html">tei:biblFull</ref></cell>
                        <cell>Enthält eine ausführliche bibliographische Angabe zur Quelle</cell>
                        <cell>optional, aber empfohlen</cell>
                        <cell>0..1</cell>
                        <cell>nein</cell>
                        <cell>ja</cell>
                        <cell>nein</cell>
                    </row>
                </table>
                <egXML xmlns="http://www.tei-c.org/ns/Examples">
                    <sourceDesc>
                        <bibl>[Bibliographischer Kurztitel der Quelle]</bibl>
                        <biblFull>
                            <titleStmt>
                                <title>[Titel, Untertitel, Band]</title>
                                <author>[Autor]</author>
                                <editor>[Herausgeber/Übersetzer]</editor>
                            </titleStmt>
                            <editionStmt>
                                <edition>[Art der zugrundeliegenden Textausgabe]</edition>
                            </editionStmt>
                            <extent>
                                <measure type="pages">[Umfang des Bandes in Seiten]</measure>
                            </extent>
                            <publicationStmt>[Angaben zur Band-Ausgabe]</publicationStmt>
                            <seriesStmt>[Angaben zur Reihe]</seriesStmt>
                        </biblFull>
                    </sourceDesc>
                </egXML>
                <p>Eine nicht-unikale Quelle wird bibliographisch in der <foreign>Source Description</foreign>
                        (<gi>teiHeader</gi>/<gi>fileDesc</gi>/<gi>sourceDesc</gi>) unter Verwendung von <gi>bibl</gi> und <gi>biblFull</gi>
                    ausgezeichnet. Das SUB TEI-P5 Anwenungsprofil folgt hier den Vorgaben des <ref
                        target="http://www.deutschestextarchiv.de/doku/basisformat/mdSourceDesc.html">DTA-Bf</ref> in Hinblick auf die
                    bibliographische Erschließung in <gi>bibl</gi> bzw. <gi>biblFull</gi>.</p>
                <p>Zur Identifikation und Beschreibung <hi rend="bold">unikaler Quellen</hi> wie Handschriften siehe Kap. <ref
                    target="#doc-msDesc">Quellenbeschreibung bei unikalen Quellen (Manuskriptbeschreibung)</ref>.</p>
                
                <div>
                    <head>Bibliographischer Kurztitel der nicht-unikalen Quelle</head>
                    <p>Der bibliographische Kurztitel der nicht-unikalen Quelle wird in <gi>sourceDesc</gi>/<gi>bibl</gi> erschlossen. Die Angabe
                        eines Kurztitels ist <hi rend="underline">verpflichtend</hi>! Es gelten die Vorgaben des <ref
                            target="http://www.deutschestextarchiv.de/doku/basisformat/mdSdBibl.html">DTA-Bf: Die Kurztitelangabe</ref>.</p>
                </div>
                
                <div>
                    <head>Ausführliche bibliographische Angabe zur nicht-unikalen Quelle</head>
                    <p>Die ausführliche bibliographische Angabe zur nicht-unikalen Quelle wird in <gi>sourceDesc</gi>/<gi>biblFull</gi> erschlossen.
                        Die Angabe ist optional! Auch hier gelten die Vorgaben des <ref
                            target="https://www.deutschestextarchiv.de/doku/basisformat/mdSdBiblFullAllg.html">DTA-Bf: Die vollständigen bibliographischen
                            Angaben zur Quelle</ref>.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>
