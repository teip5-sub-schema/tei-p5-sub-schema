<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Bibliographische Informationen zur TEI-Ressource</title>
                <author>Uwe Sikora</author>
            </titleStmt>
            <publicationStmt>
                <authority xml:id="UwS">Uwe Sikora</authority>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by/4.0"/>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Separate SUB-Schema docs-file</p>
            </sourceDesc>
        </fileDesc>
        <revisionDesc status="candidate">
            <change who="#UwS" when="2023-11-17">Linked to Schema ODD.</change>
            <change who="#UwS" when="2023-11-17">Created File.</change>
            <change who="#UwS" when="2022-01-24">Created Content (Intranet).</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <div xml:id="doc-meta-fileDesc">
                <head>Bibliographische Informationen zur TEI-Ressource</head>
                <table>
                    <row role="label">
                        <cell>Element</cell>
                        <cell>Kommentar</cell>
                        <cell>Verpflichtungsgrad</cell>
                        <cell>Kardinalität</cell>
                        <cell>DFG-DH</cell>
                        <cell>DTA-BF</cell>
                        <cell>TEI-Simple</cell>
                    </row>
                    <row>
                        <cell>
                            <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-fileDesc.html">tei:fileDesc</ref></cell>
                        <cell>Informationen über die TEI-Ressource und ihre Inhalte</cell>
                        <cell>verpflichtend</cell>
                        <cell>1</cell>
                        <cell>ja</cell>
                        <cell>ja</cell>
                        <cell>ja</cell>
                    </row>
                </table>
                <p>Bibliographische Informationen zur TEI-Ressource bzw. Edition, wie z.B. Titel- und Veröffentlichungsvermerke sowie Informationen zu
                    zugrunde liegenden Quellen, werden in der <foreign>File Description</foreign> (<gi>teiHeader</gi>/<gi>fileDesc</gi>) erfasst.<specList>
                        <specDesc key="fileDesc"/>
                    </specList></p>
                <egXML xmlns="http://www.tei-c.org/ns/Examples">
                    <fileDesc>
                        <titleStmt>[Allgemeine Titeldate der Ressource]</titleStmt>
                        <publicationStmt>[Veröffentlichungsinformationen zur Ressource]</publicationStmt>
                        <seriesStmt>[Reihenvermerk zur Ressource, sofern in einer Schriftenreihe/Zeitschrift veröffentlicht]</seriesStmt>  
                        <sourceDesc>[Beschreibung der Textquelle einer Ressource sofern vorhanden]</sourceDesc>
                    </fileDesc>
                </egXML>
                <div>
                    <head>Titelinformationen</head>
                    <table>
                        <row role="label">
                            <cell>Element</cell>
                            <cell>Kommentar</cell>
                            <cell>Verpflichtungsgrad</cell>
                            <cell>Kardinalität</cell>
                            <cell>DFG-DH</cell>
                            <cell>DTA-BF</cell>
                            <cell>TEI-Simple</cell>
                        </row>
                        <row>
                            <cell>
                                <ref target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-titleStmt.html">tei:titleStmt</ref></cell>
                            <cell>Allgemeine Titelinformationen über die TEI-Ressource</cell>
                            <cell>verpflichtend</cell>
                            <cell>1</cell>
                            <cell>ja</cell>
                            <cell>ja</cell>
                            <cell>ja</cell>
                        </row>
                    </table>
                    <p>Allgemeine Titeldaten über die TEI-Ressource werden im <foreign>Title Statment</foreign> (<gi>teiHeader</gi>/<gi>fileDesc</gi>/<gi>titleStmt</gi>)
                        angegeben. <specList>
                            <specDesc key="titleStmt"/>
                        </specList></p>
                    <p>Hierbei handelt es sich etwa um Titel- (<gi>title</gi>, siehe unter Kap. <ref target="#doc-titleStmt-title"
                        >Titelvermerke</ref>) und Autorenangaben (<gi>author</gi>, siehe unter Kap. <ref target="#doc-titleStmt-auedfu"
                            >Personenvermerke</ref>) zum edierten Werk, ganauso wie um Angaben zu Editoren (<gi>editor</gi>, siehe ebenfalls unter
                        Kap. <ref target="#doc-titleStmt-auedfu">Personenvermerke</ref>) der Edition. In diesem Rahmen können zudem weitere
                        Verantwortliche dokumentiert werden, die an der Erstellung der Edition beteiligt waren (<gi>respStmt</gi>, siehe unter Kap.
                            <ref target="#doc-respStmt">Verantwortlichkeitsvermerke</ref>).</p>
                    <note rend="warn" n="Erschließung anderer Personen mit Blick auf das Werk?">Im <gi>titleStmt</gi> wird lediglich der Autor eines
                        Werks und die Editoren der vorliegenden Edition erschlossen. Andere Personen, die mit Blick auf das Werk verantwortlich
                        zeichnen, wie etwa Übersetzer, Herausgeber etc. werden in der <ref target="#doc-sourceDesc">Quellenbeschreibung</ref>
                            (<gi>sourceDesc</gi>) angegeben.</note>
                    <egXML xmlns="http://www.tei-c.org/ns/Examples">
                        <titleStmt>
                            <title xml:lang="[Sprachcode gemäß BCP47]">[Vorzugsbenennung der Ressource]</title>
                            <title xml:lang="[Sprachcode gemäß BCP47]" type="alt">[Alternative Benennung der Ressource]</title>
                            <author>[Autorvermerk]</author>
                            <editor>[Bearbeitervermerk]</editor>
                            <funder>[Fördervermerk]</funder>
                            <respStmt>[Weiterer Verantwortlichkeitsvermerk]</respStmt>
                        </titleStmt>
                    </egXML>

                    <!-- tei:fileDesc: Titelvermerke -->
                    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="meta.titleStmt-title.xml" xpointer="element(doc-titleStmt-title)">
                        <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                    </xi:include>

                    <!-- tei:fileDesc: Personenvermerke (Author, Editor, Funder) -->
                    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="meta.titleStmt-auedfu.xml" xpointer="element(doc-titleStmt-auedfu)">
                        <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                    </xi:include>

                    <!-- tei:fileDesc: Verantwortlichkeitsvermerk -->
                    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="respStmt.xml" xpointer="element(doc-respStmt)">
                        <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                    </xi:include>
                </div>
            
                <!-- Metadaten: Veröffentlichungsvermerk -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="meta.publicationStmt.xml" xpointer="element(doc-publicationStmt)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
                
                <!-- Metadaten: Reihenvermerk -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="meta.seriesStmt.xml" xpointer="element(doc-seriesStmt)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
                
                <!-- Metadaten: Quellenbeschreibung -->
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="meta.sourceDesc.xml" xpointer="element(doc-sourceDesc)">
                    <xi:fallback>Hoops! Documentation File is missing!</xi:fallback>
                </xi:include>
            </div>
        </body>
    </text>
</TEI>
